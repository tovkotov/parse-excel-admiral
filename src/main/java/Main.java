import model.*;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import java.io.FileOutputStream;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;

public class Main {
    private static HashMap<Employee, ArrayList<StartEndTime>> workIntervals = new HashMap<>();

    public static void main(String[] args) throws IOException {
        String inFilePath1 = "data/1-9 май.xls";
//        String outFilePath = "data/_1-9 май.xls";

        String inFilePath2 = "data/10-19 май.xls";
//        String outFilePath = "data/_10-19 май.xls";

        String inFilePath3 = "data/20-27 май.xls";
//        String outFilePath = "data/_20-27 май.xls";

        String inFilePath4 = "data/28-31 май.xls";
        String outFilePath = "data/_out.xls";

        String inSheet = "Лист1";
        String sheetBase = "Лист1";
        String sheetSummary = "Лист2";

        readInFile(inFilePath1, inSheet);
        readInFile(inFilePath2, inSheet);
        readInFile(inFilePath3, inSheet);
        readInFile(inFilePath4, inSheet);

        StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure("hibernate.cfg.xml").build();

        Metadata metadata = new MetadataSources(registry).getMetadataBuilder().build();
        SessionFactory sessionFactory = metadata.getSessionFactoryBuilder().build();

        Session session = sessionFactory.openSession();

        Workbook outWorkBook = new HSSFWorkbook();

        sheet1unloading(outWorkBook, outFilePath, sheetBase);
        sheet2unloading(outWorkBook, outFilePath, sheetSummary);

        session.close();
    }

    private static void sheet1unloading(Workbook outWorkBook, String outFilePath, String sheet) throws IOException {
        Sheet sheet1 = outWorkBook.createSheet(sheet);
        int i = 0;

        Row row = sheet1.createRow(i++);
        Cell name = row.createCell(0);
        name.setCellValue("ФИО");
        Cell accessPoint = row.createCell(1);
        accessPoint.setCellValue("ТОЧКА ДОСТУПА");

        Cell start = row.createCell(2);
        start.setCellValue("ВРЕМЯ ВХОДА");

        Cell end = row.createCell(3);
        end.setCellValue("ВРЕМЯ ВЫХОДА");

        Cell duration = row.createCell(4);
        duration.setCellValue("МИН");

        for (Employee employee : workIntervals.keySet()) {
            ArrayList<StartEndTime> startEndTimes = workIntervals.get(employee);
            for (StartEndTime startEndTime : startEndTimes) {
                row = sheet1.createRow(i++);

                name = row.createCell(0);
                name.setCellValue(employee.getName());

                accessPoint = row.createCell(1);
                if (startEndTime.getAccessPoint() != null)
                    accessPoint.setCellValue(startEndTime.getAccessPoint().name());

                start = row.createCell(2);
                if (startEndTime.getStartTime() != null)
                    start.setCellValue(startEndTime.getStartTime().toString());

                end = row.createCell(3);
                if (startEndTime.getEndTime() != null)
                    end.setCellValue(startEndTime.getEndTime().toString());

                if ((startEndTime.getStartTime() != null) &&
                        (startEndTime.getEndTime() != null)) {
                    long mins = ChronoUnit.MINUTES.between(startEndTime.getStartTime(), startEndTime.getEndTime());
                    duration = row.createCell(4);
                    duration.setCellValue(mins);
                }

                Interval interval = new Interval();
                interval.setEmployee(employee);
                interval.setStartTime(startEndTime.getStartTime());
                interval.setEndTime(startEndTime.getEndTime());
                interval.setAccessPoint(startEndTime.getAccessPoint());
                //  session.save(interval);
            }
        }

        sheet1.autoSizeColumn(1);
        outWorkBook.write(new FileOutputStream(outFilePath));
        outWorkBook.close();
    }

    private static void sheet2unloading(Workbook outWorkBook, String outFilePath, String sheet) throws IOException {
        Sheet sheet2 = outWorkBook.createSheet(sheet);
        int i = 0;
        Row row = sheet2.createRow(i++);
        Cell date = row.createCell(0);
        date.setCellValue("ДАТА");

        Cell employeeName = row.createCell(1);
        employeeName.setCellValue("ИМЯ");

        Cell orderName = row.createCell(2);
        orderName.setCellValue("ЗАКАЗ");

        Cell durationMin = row.createCell(3);
        durationMin.setCellValue("ИТОГО НА ЗАКАЗЕ, МИН");

        Cell durationHourMin = row.createCell(4);
        durationHourMin.setCellValue("ИТОГО НА ЗАКАЗЕ, ЧЧ:MM");

        HashMap<DateOrder, Long> summary = new HashMap<>();
        ArrayList<Errors> errorList = new ArrayList<>();
        for (Employee employee : workIntervals.keySet()) {
            ArrayList<StartEndTime> startEndTimes = workIntervals.get(employee);
            //ArrayList<StartEndTime> errors = new ArrayList<>();
            for (StartEndTime startEndTime : startEndTimes) {
                DateOrder dateOrder = new DateOrder();
                long mins = 0;
                if ((startEndTime.getStartTime() != null) &&
                        (startEndTime.getEndTime() != null)) {
                    mins = ChronoUnit.MINUTES.between(startEndTime.getStartTime(), startEndTime.getEndTime());

                    dateOrder.setDate(startEndTime.getStartTime().toLocalDate());
                    dateOrder.setEmployee(employee);
                    dateOrder.setAccessPoint(startEndTime.getAccessPoint());

                } else {
                    errorList.add(new Errors(employee, startEndTime));
                }

                if (summary.containsKey(dateOrder)) {
                    mins += summary.get(dateOrder);
                }
                summary.put(dateOrder, mins);
            }

        }

        for (DateOrder dateOrder : summary.keySet()) {
            row = sheet2.createRow(i++);
            date = row.createCell(0);
            if (dateOrder.getDate() != null) {
                date.setCellValue(dateOrder.getDate().toString());
            } else continue;


            employeeName = row.createCell(1);
            if (dateOrder.getEmployee() != null) {
                employeeName.setCellValue(dateOrder.getEmployee().getName());
            } else continue;

            orderName = row.createCell(2);
            if (dateOrder.getAccessPoint() != null) {
                orderName.setCellValue(dateOrder.getAccessPoint().name());
            } else continue;

            durationMin = row.createCell(3);
            durationMin.setCellValue(summary.get(dateOrder));

            durationHourMin = row.createCell(4);

            LocalTime localTime = LocalTime.MIN.plus(Duration.ofMinutes(summary.get(dateOrder)));
            durationHourMin.setCellValue(localTime.toString());
        }

        Sheet sheet3 = outWorkBook.createSheet("Ошибки");

        i = 0;
        row = sheet3.createRow(i++);
        Cell emplCell = row.createCell(0);
        emplCell.setCellValue("ИМЯ");


        Cell order = row.createCell(1);
        order.setCellValue("ЗАКАЗ");

        Cell startCell = row.createCell(2);
        startCell.setCellValue("ВРЕМЯ ВХОДА");

        Cell endCell = row.createCell(3);
        endCell.setCellValue("ВРЕМЯ ВЫХОДА");


        for (Errors error : errorList) {
            row = sheet3.createRow(i++);

            if (error.getEmployee() != null) {
                emplCell = row.createCell(0);
                emplCell.setCellValue(error.getEmployee().getName());
            }

            StartEndTime startEndTime = error.getStartEndTime();
            if (startEndTime.getAccessPoint() != null) {
                order = row.createCell(1);
                order.setCellValue(startEndTime.getAccessPoint().name());
            }

            if (startEndTime.getStartTime() != null) {
                startCell = row.createCell(2);
                startCell.setCellValue(startEndTime.getStartTime().toString());
            }

            if (startEndTime.getEndTime() != null) {
                endCell = row.createCell(3);
                endCell.setCellValue(startEndTime.getEndTime().toString());
            }
        }


        // рассчет листа Итого
        Sheet sheet4 = outWorkBook.createSheet("Итого");
        i = 0;
        row = sheet4.createRow(i++);
        emplCell = row.createCell(0);
        emplCell.setCellValue("ИМЯ");

        order = row.createCell(1);
        order.setCellValue("ЗАКАЗ");

        Cell durationCell = row.createCell(2);
        durationCell.setCellValue("Итого на заказе, мин");

        Cell durationTCell = row.createCell(3);
        durationTCell.setCellValue("Итого на заказе, час:мин");

        for (Employee employee : workIntervals.keySet()) {
            HashMap<AccessPoint, Long> totalDuration = new HashMap<>();
            for (StartEndTime startEndTime : workIntervals.get(employee)) {
                if (startEndTime.getStartTime() == null){
                    continue;
                }
                if (startEndTime.getEndTime() == null){
                    continue;
                }
                long min = ChronoUnit.MINUTES.between(startEndTime.getStartTime(), startEndTime.getEndTime());
                long total = totalDuration.get(startEndTime.getAccessPoint()) == null ? min :
                        totalDuration.get(startEndTime.getAccessPoint()) + min;
                totalDuration.put(startEndTime.getAccessPoint(), total);
            }
            for (AccessPoint accessPoint : totalDuration.keySet()){
                row = sheet4.createRow(i++);
                emplCell = row.createCell(0);
                emplCell.setCellValue(employee.getName());
                order = row.createCell(1);
                order.setCellValue(accessPoint.name());
                durationCell = row.createCell(2);
                durationCell.setCellValue(totalDuration.get(accessPoint));

                durationTCell = row.createCell(3);
                Duration duration = Duration.ofMinutes(totalDuration.get(accessPoint));
                //LocalDateTime localDateTime = LocalDateTime.MIN.plus(Duration.ofMinutes(totalDuration.get(accessPoint)));
                durationTCell.setCellValue(duration.toHours() + ":" + duration.toMinutesPart());
            }
        }

        outWorkBook.write(new FileOutputStream(outFilePath));
        outWorkBook.close();
        //session.close();
    }

    private static void readInFile(String inFile, String sheet) {
        ParseExcel readWriteExcel = new ParseExcel(inFile, sheet);
        HashMap<Employee, ArrayList<StartEndTime>> workIntervals1 = readWriteExcel.getEmployeeIntervals();
        //readWriteExcel.getEmployeeIntervals().putAll(workIntervals);
        workIntervals.putAll(workIntervals1);
    }

}
