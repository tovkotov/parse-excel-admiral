package model;

import lombok.Data;

import java.time.LocalDate;

@Data
public class DateOrder {
    private LocalDate date;
    private AccessPoint accessPoint;
    private Employee employee;
    private long duration;
}
