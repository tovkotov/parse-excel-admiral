package model;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class StartEndTime {
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private AccessPoint accessPoint;
}
