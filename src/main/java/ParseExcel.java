import lombok.Data;
import model.AccessPoint;
import model.Employee;
import model.StartEndTime;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;

import java.io.FileInputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

@Data
public class ParseExcel {
    private static final int START_ROW = 1; // начало данных в файле с этой строки
    private final int DATE_COLUMN = 0;
    private final int TIME_COLUMN = 1;
    private final int ACCESS_POINT_COLUMN = 2;
    private final int ACCESS_EVENT_COLUMN = 3;
    private final int ACCESS_EVENT_DETAIL_COLUMN = 4;
    private final int EMPLOYEE_NAME_COLUMN = 5;

    private String fileName;
    private TreeMap<String, Employee> employeeList;
    private String sheetName;
    HashMap<Employee, ArrayList<StartEndTime>> employeeIntervals;

    public ParseExcel(String fileName, String sheetName) {
        this.fileName = fileName;
        this.sheetName = sheetName;
        employeeList = new TreeMap<>();
        //  workTimes = new ArrayList<>();
        employeeIntervals = new HashMap<>();
        readFromExcel();
    }

    public void readFromExcel() {
        int count = 0;
        try {
            FileInputStream file = new FileInputStream(fileName);
            HSSFWorkbook workbook = new HSSFWorkbook(file);
            HSSFSheet sheet = workbook.getSheet(sheetName);

            Employee employee;
            LocalDateTime localDateTime;

            String date;
            ArrayList<StartEndTime> intervals;
            StartEndTime startEndTime;

            for (Row row : sheet) {
                count++;

                if (row.getRowNum() < START_ROW) continue;

                if (row.getCell(DATE_COLUMN).getStringCellValue().isEmpty()) {
                    System.out.println("обработано строк: " + count);
                    break;
                }

//                if (row.getCell(ACCESS_EVENT_DETAIL_COLUMN).getStringCellValue().equals("Отказ от доступа.")) {
//                    continue;
//                }

                //if (!row.getCell(0).getStringCellValue().isEmpty()) {
                date = row.getCell(DATE_COLUMN).getStringCellValue();
                employee = getEmployee(row.getCell(EMPLOYEE_NAME_COLUMN).getStringCellValue());
                intervals = getIntervals(employee);

                String dateTime = date + " " + row.getCell(TIME_COLUMN).getStringCellValue();
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                //System.out.println(dateTime);

                localDateTime = LocalDateTime.parse(dateTime, formatter);
                //}

                AccessPoint accessPoint = AccessPoint.getAccessPoint(row.getCell(ACCESS_POINT_COLUMN).getStringCellValue());
                if (accessPoint == null) {
                    continue; // такая строка нас не интересует, пропускаем все что ниже, идем на новую строку
                }

                if (row.getCell(ACCESS_EVENT_COLUMN).getStringCellValue().equals("вход")) {
                    addStart(employee, accessPoint, localDateTime);
                } else {
                    addEnd(employee, accessPoint, localDateTime);
                }
                // тут бы проверить аксесс поинт а вдруг он другой??
                //?startEndTime.setAccessPoint(accessPoint);
                //if (newInterval)
                //intervals.add(startEndTime);
            }
        } catch (Exception e) {
            System.out.println("Ошибка в строке: " + count);
            e.printStackTrace();
        }
    }

    private void addStart(Employee employee, AccessPoint accessPoint, LocalDateTime localDateTime) {
        ArrayList<StartEndTime> intervals = getIntervals(employee);
        StartEndTime startEndTime = new StartEndTime();
        startEndTime.setStartTime(localDateTime);
        startEndTime.setAccessPoint(accessPoint);
        intervals.add(startEndTime);
        //return startEndTime;
    }

    private void addEnd(Employee employee, AccessPoint accessPoint, LocalDateTime endTime) {
        ArrayList<StartEndTime> intervals = getIntervals(employee);
        StartEndTime startEndTime;

        if (intervals.isEmpty()) {
            StartEndTime startEndTime1 = new StartEndTime();
            startEndTime1.setEndTime(endTime);
            startEndTime1.setAccessPoint(accessPoint);
            intervals.add(startEndTime1);
            return;
        }
        startEndTime = intervals.get(intervals.size() - 1);
        if (startEndTime.getStartTime() != null) {
            long days = ChronoUnit.DAYS.between(startEndTime.getStartTime(), endTime);
            if ((days > 1) | (!startEndTime.getAccessPoint().equals(accessPoint))) {
                startEndTime = new StartEndTime();
                startEndTime.setEndTime(endTime);
                startEndTime.setAccessPoint(accessPoint);
                intervals.add(startEndTime);
                //return startEndTime;
            } else {
                startEndTime.setEndTime(endTime);
                startEndTime.setAccessPoint(accessPoint);
            }

        } else {
            startEndTime = new StartEndTime();
            startEndTime.setEndTime(endTime);
            startEndTime.setAccessPoint(accessPoint);
            intervals.add(startEndTime);
        }

        //intervals.add(startEndTime);
//        return startEndTime;
    }


    private ArrayList<StartEndTime> getIntervals(Employee employee) {
        if (employeeIntervals.containsKey(employee)) {
            return employeeIntervals.get(employee);
        }
        ArrayList<StartEndTime> arrayList = new ArrayList<>();
        employeeIntervals.put(employee, arrayList);
        return arrayList;
    }

    private Employee getEmployee(String employeeName) {
        if (employeeList.containsKey(employeeName)) {
            return employeeList.get(employeeName);
        }
        Employee employee = new Employee(employeeName);
        employeeList.put(employeeName, employee);
        return employee;
    }
}
